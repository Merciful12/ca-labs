export default class Algorithm {
  static countOfWays(coins: number[], countsSel: number[], countCus: number[], idx: number, sum: number): number {
    if (sum == 0)
      return 1

    if (sum < 0) {
      if (countCus && countCus.length) {
        return this.countOfWays(coins, countCus, null, 7, Math.abs(sum))
      }
      return 0
    }

    if (idx <= 0 && sum >= 1)
      return 0

    let include = 0
    if (countsSel[idx - 1] > 0) {
      countsSel[idx - 1]--
      include = this.countOfWays(coins, countsSel, countCus, idx, sum - coins[idx - 1])
      countsSel[idx - 1]++
    }
    let exclude = this.countOfWays(coins, countsSel, countCus, idx - 1, sum)

    return include + exclude
  }
}
