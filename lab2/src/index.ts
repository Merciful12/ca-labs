import Algorithm from './Algorithm/Algorithm'

const selects = document.getElementsByClassName('inputs')

const coins = [100, 200, 500, 1, 5, 10, 50]


document.getElementById('start').addEventListener('click', function(e) {
  let values: number[] = [];
  [].forEach.call(selects, (e: HTMLElement) => {
    values.push(+(<HTMLInputElement>e).value)
  })
  
  const lenHalf = values.length / 2
  const seller = values.slice(0, lenHalf)
  const customer = values.slice(lenHalf)

  const rest: number = 
    +(<HTMLInputElement>document.getElementById('rest-ruble')).value * 100 +
    +(<HTMLInputElement>document.getElementById('rest-penny')).value

  let variants = Algorithm.countOfWays(coins,seller, customer, 7,rest)

  const res = document.getElementById('main')
  if (variants == 0)
  {
      res.innerText = 'we cant give a rest'
  } else {
      res.innerText = 'variants:' + variants.toString()
  }
})
