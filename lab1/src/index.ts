import Puzzle from './Puzzle/Puzzle'

const puzzleColor:HTMLElement = document.getElementById('puzzle-colors')
const result:HTMLElement = document.getElementById('puzzle-result')

const start:HTMLElement = document.getElementById('start')

start.addEventListener('click', function(e) {
  puzzleColor.innerHTML = ''
  result.innerHTML = ''
  const size:number =  +(<HTMLSelectElement>document.getElementById('size')).value
  const puzzle = new Puzzle(size, size, puzzleColor, result)
  puzzle.drawMatrix()
  puzzle.randomGeneratePuzzle()
  if (puzzle.start()) {
    puzzle.drawPuzzleFinish()
  } else {
    alert('puzzle wasnt collected')
  }
})
