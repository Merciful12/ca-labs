export default class Permutator {
    private n: number   // кол-во элементов 
    nums: number[]  // массив переставляемых элементов

    constructor(n: number) {
        this.n = n
        this.nums = <number[]>new Array(n)
        for (let i = 0; i < n; i++) {
            this.nums[i] = i + 1
        }
    }

    private swap(i: number, j: number) {
        const tmp = this.nums[i]
        this.nums[i] = this.nums[j]
        this.nums[j] = tmp
    }

    nextSet(): boolean {
        let j = this.n - 2
        while (j != -1 && this.nums[j] >= this.nums[j + 1]) j--
        if (j == -1)
            return false // больше перестановок нет
        let k = this.n - 1
        while (this.nums[j] >= this.nums[k]) k--

        this.swap(j, k)

        let l = j + 1, r = this.n - 1 // сортируем оставшуюся часть последовательности
        while (l < r) {
            this.swap(l++, r--)
        }
        return true
    }
}