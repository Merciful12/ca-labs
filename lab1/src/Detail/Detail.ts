export enum MyColor {
  red,
  green,
  blue
}

export enum TypeSide {
  STRAIGHT_SIDE = 0,
  TRIANGLE_SIDE = 1,
  REVERSE_TRIANGLE_SIDE = -1,
  SQUARE_SIDE = 2,
  REVERSE_SQUARE_SIDE = -2,
  NONE_SIDE = -3
}

export default class Detail {
  color: MyColor
  leftSide: TypeSide
  rightSide: TypeSide
  upSide: TypeSide
  downSide: TypeSide

  constructor(color: MyColor, leftSide: TypeSide,
    rightSide: TypeSide, upSide: TypeSide, downSide: TypeSide) {
    this.color = color
    this.leftSide = leftSide
    this.rightSide = rightSide
    this.upSide = upSide
    this.downSide = downSide
  }

  draw(): HTMLElement {
    const rect:HTMLElement = document.createElement('div')
    rect.classList.add('detail', MyColor[this.color])
    return rect
  }

  static equalsToPuzzle(tpOne: TypeSide, tpTwo: TypeSide):boolean {
    switch (tpOne) {
      case TypeSide.SQUARE_SIDE:
        return tpTwo === TypeSide.REVERSE_SQUARE_SIDE
      case TypeSide.REVERSE_SQUARE_SIDE:
        return (tpTwo === TypeSide.SQUARE_SIDE)
      case TypeSide.REVERSE_TRIANGLE_SIDE:
        return (tpTwo === TypeSide.TRIANGLE_SIDE)
      case TypeSide.TRIANGLE_SIDE:
        return (tpTwo === TypeSide.REVERSE_TRIANGLE_SIDE)
      case TypeSide.STRAIGHT_SIDE:
        return (tpTwo === TypeSide.STRAIGHT_SIDE)
    }
  }

  static reverseSide(type: TypeSide): TypeSide {
    switch (type) {
      case TypeSide.SQUARE_SIDE:
        return TypeSide.REVERSE_SQUARE_SIDE
      case TypeSide.REVERSE_SQUARE_SIDE:
        return TypeSide.SQUARE_SIDE
      case TypeSide.REVERSE_TRIANGLE_SIDE:
        return TypeSide.TRIANGLE_SIDE
      case TypeSide.TRIANGLE_SIDE:
        return TypeSide.REVERSE_TRIANGLE_SIDE
      case TypeSide.STRAIGHT_SIDE:
        return TypeSide.STRAIGHT_SIDE
      default:
        return TypeSide.NONE_SIDE
    }
  }
}