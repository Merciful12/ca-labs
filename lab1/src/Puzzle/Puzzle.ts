import Detail, { MyColor, TypeSide } from '../Detail/Detail'
import Permutator from '../Permutator/Permutator'

type RandomFnc = (min: number, max: number) => number
const random: RandomFnc = (min: number = 0, max) => Math.floor(Math.random() * (max - min)) + min;

export default class Puzzle {
    private arrayDetails: Detail[]  // массив деталей
    private perm: number[]  // массив номеров элементов (в который заносится текущая перестановка)
    private countColomns: number  // количество столбцов
    private countLines: number // количество строк
    private countElem: number // кол-во элементов
    private matrColor: MyColor[]  // матрица цветов

    private puzzleColor: HTMLElement
    private result: HTMLElement

    constructor(_countColomns: number, _countLines: number,
        puzzleColor: HTMLElement, result: HTMLElement) {
        this.countColomns = _countColomns
        this.countLines = _countLines
        this.countElem = _countColomns * _countLines
        this.puzzleColor = puzzleColor
        this.result = result

        this.arrayDetails = new Array(this.countElem)
        this.initMatrixColor()
    }


    private initMatrixColor(): void {
        this.matrColor = new Array(this.countElem)
        
        for (let i = 0; i < this.countElem; i++)
            this.matrColor[i] = <MyColor>random(0, 3)

    }

    drawMatrix(): void {
        for (let i = 0; i < this.countLines; i++) {
            const row: HTMLDivElement = document.createElement('div')
            row.classList.add('row')
            for (let j = 0; j < this.countColomns; j++) {
                const color = this.matrColor[i * this.countColomns + j]
                const detail = new Detail(color, -1, -2, 0, 1)
                row.appendChild(detail.draw())
            }
            this.puzzleColor.appendChild(row)
        }
    }
    /// Рандомная генерация правильного пазла. Потом его надо перемешать.
    randomGeneratePuzzle(): void {
        for (let i = 0; i < this.countElem; i++) {
            let leftSide: TypeSide = TypeSide.NONE_SIDE // тип левой стороны
            let rightSide: TypeSide = TypeSide.NONE_SIDE  // тип правой стороны
            let upSide: TypeSide = TypeSide.NONE_SIDE  // тип верхней стороны
            let downSide: TypeSide = TypeSide.NONE_SIDE  // тип нижней стороны
            if ((i >= 0) && (i < this.countColomns))
                upSide = TypeSide.STRAIGHT_SIDE
            if ((i >= (this.countElem - this.countColomns)) && (i < this.countElem))
                downSide = TypeSide.STRAIGHT_SIDE
            if ((i % this.countColomns) == 0)
                leftSide = TypeSide.STRAIGHT_SIDE
            if ((i % this.countColomns) == (this.countColomns - 1))
                rightSide = TypeSide.STRAIGHT_SIDE
            if ((i == 0)) {
                if (rightSide == TypeSide.NONE_SIDE)
                    rightSide = <TypeSide>random(-2, 2)
                if (downSide == TypeSide.NONE_SIDE)
                    downSide = <TypeSide>random(-2, 2)
            }
            if ((i > 0) && (i < this.countColomns)) {
                if (leftSide == TypeSide.NONE_SIDE)
                    leftSide = Detail.reverseSide(this.arrayDetails[i - 1].rightSide)
                if (rightSide == TypeSide.NONE_SIDE)
                    rightSide = <TypeSide>random(-2, 2)
                if (downSide == TypeSide.NONE_SIDE)
                    downSide = <TypeSide>random(-2, 2)
            }
            if ((i >= this.countColomns) && (i < this.countElem)) {
                if (leftSide == TypeSide.NONE_SIDE)
                    leftSide = Detail.reverseSide(this.arrayDetails[i - 1].rightSide)
                if (upSide == TypeSide.NONE_SIDE)
                    upSide = Detail.reverseSide(this.arrayDetails[i - this.countColomns].downSide)
                if (rightSide == TypeSide.NONE_SIDE)
                    rightSide = <TypeSide>random(-2, 2)
                if (downSide == TypeSide.NONE_SIDE)
                    downSide = <TypeSide>random(-2, 2)
            }
            this.arrayDetails[i] = new Detail(this.matrColor[i], leftSide, rightSide, upSide, downSide)
        }

        this.matrixMix()
    }

    public start(): boolean {
        let pr: Permutator = new Permutator(this.countElem)
        let res = true
        do {
          this.perm = [...pr.nums]
          for (let i = 0; i < this.perm.length; i++) --this.perm[i]
          if (this.check()) {
            return true;
          }
          res = pr.nextSet()
        } while (res);
        return false;
    }

    drawPuzzleFinish(): void {
        for (let i = 0; i < this.countLines; i++) {
            const row: HTMLDivElement = document.createElement('div')
            row.classList.add('row')
            for (let j = 0; j < this.countColomns; j++) {
                const detail = this.arrayDetails[this.perm[i * this.countColomns + j]]
                row.appendChild(detail.draw())
            }
            this.result.appendChild(row)
        }
    }

    private matrixMix(): void {
        for (let i = this.arrayDetails.length - 1; i >= 1; i--) {
            const j = random(0, i + 1);
            // обменять значения arrayDetails[j] и arrayDetails[i]
            [this.arrayDetails[j], this.arrayDetails[i]] = [this.arrayDetails[i], this.arrayDetails[j]]
        }
    }


    check(): boolean {
        return this.perm.every((v,i) => this.checkDetails(i))
    }

    checkDetails(id: number): boolean {        
        if (this.arrayDetails[this.perm[id]].color != this.matrColor[id])
            return false;
        if ((id >= 0) && (id < this.countColomns)) {
            if (this.arrayDetails[this.perm[id]].upSide != TypeSide.STRAIGHT_SIDE)
                return false;
            if ((id >= 1) && (!Detail.equalsToPuzzle(this.arrayDetails[this.perm[id]].leftSide, this.arrayDetails[this.perm[id - 1]].rightSide)))
                return false;
        }

        if ((id >= (this.countElem - this.countColomns)) && (id < this.countElem)) {
            if (this.arrayDetails[this.perm[id]].downSide != TypeSide.STRAIGHT_SIDE)
                return false
        }
        if (id == 0) {
            if (this.arrayDetails[this.perm[id]].leftSide != TypeSide.STRAIGHT_SIDE)
                return false
        }

        if (id == (this.countElem - 1)) {
            if (this.arrayDetails[this.perm[id]].rightSide != TypeSide.STRAIGHT_SIDE)
                return false
        }

        if ((id >= this.countColomns) && (id < this.countElem)) {
            if (!Detail.equalsToPuzzle(this.arrayDetails[this.perm[id]].leftSide, this.arrayDetails[this.perm[id - 1]].rightSide))
                return false
            if (!Detail.equalsToPuzzle(this.arrayDetails[this.perm[id]].upSide, this.arrayDetails[this.perm[id - this.countColomns]].downSide))
                return false
        }

        return true

    }

}