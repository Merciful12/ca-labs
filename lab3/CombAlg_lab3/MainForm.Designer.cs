﻿namespace CombAlg_lab3
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.OrigLV = new System.Windows.Forms.ListView();
            this.Name_1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Weigth = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Price = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.LVPermut = new System.Windows.Forms.ListView();
            this.Name_2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Weigth_1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Price_1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblOrig = new System.Windows.Forms.Label();
            this.lblPerm = new System.Windows.Forms.Label();
            this.btnRun = new System.Windows.Forms.Button();
            this.lblMaxWeigth = new System.Windows.Forms.Label();
            this.tbMaxW = new System.Windows.Forms.TextBox();
            this.lblGen = new System.Windows.Forms.Label();
            this.LVGen = new System.Windows.Forms.ListView();
            this.Name_3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Weigth_3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Price_3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblTimePerm = new System.Windows.Forms.Label();
            this.tbTimePerm = new System.Windows.Forms.TextBox();
            this.lblTimeGen = new System.Windows.Forms.Label();
            this.tbTimeGen = new System.Windows.Forms.TextBox();
            this.lblForGen = new System.Windows.Forms.Label();
            this.lblCntSteps = new System.Windows.Forms.Label();
            this.nudCntSteps = new System.Windows.Forms.NumericUpDown();
            this.lblCntChr = new System.Windows.Forms.Label();
            this.nudCntChr = new System.Windows.Forms.NumericUpDown();
            this.lblCntParChr = new System.Windows.Forms.Label();
            this.nudCntParChr = new System.Windows.Forms.NumericUpDown();
            this.lblCntMutChr = new System.Windows.Forms.Label();
            this.nudCnrMutChr = new System.Windows.Forms.NumericUpDown();
            this.lblProcMut = new System.Windows.Forms.Label();
            this.nudMutProc = new System.Windows.Forms.NumericUpDown();
            this.AddItemBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudCntSteps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCntChr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCntParChr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCnrMutChr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMutProc)).BeginInit();
            this.SuspendLayout();
            // 
            // OrigLV
            // 
            this.OrigLV.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Name_1,
            this.Weigth,
            this.Price});
            this.OrigLV.Location = new System.Drawing.Point(11, 45);
            this.OrigLV.Margin = new System.Windows.Forms.Padding(2);
            this.OrigLV.Name = "OrigLV";
            this.OrigLV.Size = new System.Drawing.Size(298, 184);
            this.OrigLV.TabIndex = 0;
            this.OrigLV.UseCompatibleStateImageBehavior = false;
            this.OrigLV.View = System.Windows.Forms.View.Details;
            // 
            // Name_1
            // 
            this.Name_1.Text = "Название";
            this.Name_1.Width = 120;
            // 
            // Weigth
            // 
            this.Weigth.Text = "Вес";
            this.Weigth.Width = 120;
            // 
            // Price
            // 
            this.Price.Text = "Стоимость";
            this.Price.Width = 120;
            // 
            // LVPermut
            // 
            this.LVPermut.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Name_2,
            this.Weigth_1,
            this.Price_1});
            this.LVPermut.Location = new System.Drawing.Point(11, 258);
            this.LVPermut.Margin = new System.Windows.Forms.Padding(2);
            this.LVPermut.Name = "LVPermut";
            this.LVPermut.Size = new System.Drawing.Size(298, 154);
            this.LVPermut.TabIndex = 1;
            this.LVPermut.UseCompatibleStateImageBehavior = false;
            this.LVPermut.View = System.Windows.Forms.View.Details;
            // 
            // Name_2
            // 
            this.Name_2.Text = "Название";
            this.Name_2.Width = 133;
            // 
            // Weigth_1
            // 
            this.Weigth_1.Text = "Вес";
            this.Weigth_1.Width = 98;
            // 
            // Price_1
            // 
            this.Price_1.Text = "Стоимость";
            this.Price_1.Width = 147;
            // 
            // lblOrig
            // 
            this.lblOrig.AutoSize = true;
            this.lblOrig.Location = new System.Drawing.Point(9, 19);
            this.lblOrig.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOrig.Name = "lblOrig";
            this.lblOrig.Size = new System.Drawing.Size(99, 13);
            this.lblOrig.TabIndex = 2;
            this.lblOrig.Text = "Исходные данные";
            // 
            // lblPerm
            // 
            this.lblPerm.AutoSize = true;
            this.lblPerm.Location = new System.Drawing.Point(9, 242);
            this.lblPerm.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPerm.Name = "lblPerm";
            this.lblPerm.Size = new System.Drawing.Size(107, 13);
            this.lblPerm.TabIndex = 3;
            this.lblPerm.Text = "Перебор вариантов";
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(328, 193);
            this.btnRun.Margin = new System.Windows.Forms.Padding(2);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(102, 25);
            this.btnRun.TabIndex = 4;
            this.btnRun.Text = "Найти решение";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // lblMaxWeigth
            // 
            this.lblMaxWeigth.AutoSize = true;
            this.lblMaxWeigth.Location = new System.Drawing.Point(326, 19);
            this.lblMaxWeigth.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMaxWeigth.Name = "lblMaxWeigth";
            this.lblMaxWeigth.Size = new System.Drawing.Size(73, 13);
            this.lblMaxWeigth.TabIndex = 5;
            this.lblMaxWeigth.Text = "Вес рюкзака";
            // 
            // tbMaxW
            // 
            this.tbMaxW.Location = new System.Drawing.Point(399, 16);
            this.tbMaxW.Margin = new System.Windows.Forms.Padding(2);
            this.tbMaxW.Name = "tbMaxW";
            this.tbMaxW.Size = new System.Drawing.Size(76, 20);
            this.tbMaxW.TabIndex = 6;
            // 
            // lblGen
            // 
            this.lblGen.AutoSize = true;
            this.lblGen.Location = new System.Drawing.Point(364, 242);
            this.lblGen.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGen.Name = "lblGen";
            this.lblGen.Size = new System.Drawing.Size(128, 13);
            this.lblGen.TabIndex = 7;
            this.lblGen.Text = "Генетический алгоритм";
            // 
            // LVGen
            // 
            this.LVGen.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Name_3,
            this.Weigth_3,
            this.Price_3});
            this.LVGen.Location = new System.Drawing.Point(366, 258);
            this.LVGen.Margin = new System.Windows.Forms.Padding(2);
            this.LVGen.Name = "LVGen";
            this.LVGen.Size = new System.Drawing.Size(302, 154);
            this.LVGen.TabIndex = 8;
            this.LVGen.UseCompatibleStateImageBehavior = false;
            this.LVGen.View = System.Windows.Forms.View.Details;
            // 
            // Name_3
            // 
            this.Name_3.Text = "Название";
            // 
            // Weigth_3
            // 
            this.Weigth_3.Text = "Вес";
            // 
            // Price_3
            // 
            this.Price_3.Text = "Стоимость";
            // 
            // lblTimePerm
            // 
            this.lblTimePerm.AutoSize = true;
            this.lblTimePerm.Location = new System.Drawing.Point(9, 424);
            this.lblTimePerm.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTimePerm.Name = "lblTimePerm";
            this.lblTimePerm.Size = new System.Drawing.Size(40, 13);
            this.lblTimePerm.TabIndex = 9;
            this.lblTimePerm.Text = "Время";
            // 
            // tbTimePerm
            // 
            this.tbTimePerm.Location = new System.Drawing.Point(11, 440);
            this.tbTimePerm.Margin = new System.Windows.Forms.Padding(2);
            this.tbTimePerm.Name = "tbTimePerm";
            this.tbTimePerm.ReadOnly = true;
            this.tbTimePerm.Size = new System.Drawing.Size(92, 20);
            this.tbTimePerm.TabIndex = 10;
            // 
            // lblTimeGen
            // 
            this.lblTimeGen.AutoSize = true;
            this.lblTimeGen.Location = new System.Drawing.Point(364, 424);
            this.lblTimeGen.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTimeGen.Name = "lblTimeGen";
            this.lblTimeGen.Size = new System.Drawing.Size(40, 13);
            this.lblTimeGen.TabIndex = 11;
            this.lblTimeGen.Text = "Время";
            // 
            // tbTimeGen
            // 
            this.tbTimeGen.Location = new System.Drawing.Point(366, 440);
            this.tbTimeGen.Margin = new System.Windows.Forms.Padding(2);
            this.tbTimeGen.Name = "tbTimeGen";
            this.tbTimeGen.ReadOnly = true;
            this.tbTimeGen.Size = new System.Drawing.Size(92, 20);
            this.tbTimeGen.TabIndex = 12;
            // 
            // lblForGen
            // 
            this.lblForGen.AutoSize = true;
            this.lblForGen.Location = new System.Drawing.Point(326, 45);
            this.lblForGen.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblForGen.Name = "lblForGen";
            this.lblForGen.Size = new System.Drawing.Size(162, 13);
            this.lblForGen.TabIndex = 13;
            this.lblForGen.Text = "Для генетического алгоритма";
            // 
            // lblCntSteps
            // 
            this.lblCntSteps.AutoSize = true;
            this.lblCntSteps.Location = new System.Drawing.Point(326, 67);
            this.lblCntSteps.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCntSteps.Name = "lblCntSteps";
            this.lblCntSteps.Size = new System.Drawing.Size(100, 13);
            this.lblCntSteps.TabIndex = 14;
            this.lblCntSteps.Text = "Количество шагов";
            // 
            // nudCntSteps
            // 
            this.nudCntSteps.Location = new System.Drawing.Point(428, 65);
            this.nudCntSteps.Margin = new System.Windows.Forms.Padding(2);
            this.nudCntSteps.Name = "nudCntSteps";
            this.nudCntSteps.Size = new System.Drawing.Size(69, 20);
            this.nudCntSteps.TabIndex = 15;
            // 
            // lblCntChr
            // 
            this.lblCntChr.AutoSize = true;
            this.lblCntChr.Location = new System.Drawing.Point(326, 93);
            this.lblCntChr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCntChr.Name = "lblCntChr";
            this.lblCntChr.Size = new System.Drawing.Size(120, 13);
            this.lblCntChr.TabIndex = 16;
            this.lblCntChr.Text = "Количество хромосом";
            // 
            // nudCntChr
            // 
            this.nudCntChr.Location = new System.Drawing.Point(446, 93);
            this.nudCntChr.Margin = new System.Windows.Forms.Padding(2);
            this.nudCntChr.Name = "nudCntChr";
            this.nudCntChr.Size = new System.Drawing.Size(71, 20);
            this.nudCntChr.TabIndex = 17;
            // 
            // lblCntParChr
            // 
            this.lblCntParChr.AutoSize = true;
            this.lblCntParChr.Location = new System.Drawing.Point(326, 115);
            this.lblCntParChr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCntParChr.Name = "lblCntParChr";
            this.lblCntParChr.Size = new System.Drawing.Size(269, 13);
            this.lblCntParChr.TabIndex = 18;
            this.lblCntParChr.Text = "Кол-во отбираемых хромосом в каждом поколении";
            // 
            // nudCntParChr
            // 
            this.nudCntParChr.Location = new System.Drawing.Point(588, 115);
            this.nudCntParChr.Margin = new System.Windows.Forms.Padding(2);
            this.nudCntParChr.Name = "nudCntParChr";
            this.nudCntParChr.Size = new System.Drawing.Size(68, 20);
            this.nudCntParChr.TabIndex = 19;
            // 
            // lblCntMutChr
            // 
            this.lblCntMutChr.AutoSize = true;
            this.lblCntMutChr.Location = new System.Drawing.Point(326, 139);
            this.lblCntMutChr.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCntMutChr.Name = "lblCntMutChr";
            this.lblCntMutChr.Size = new System.Drawing.Size(161, 13);
            this.lblCntMutChr.TabIndex = 20;
            this.lblCntMutChr.Text = "Кол-во мутирующих хромосом";
            // 
            // nudCnrMutChr
            // 
            this.nudCnrMutChr.Location = new System.Drawing.Point(484, 139);
            this.nudCnrMutChr.Margin = new System.Windows.Forms.Padding(2);
            this.nudCnrMutChr.Name = "nudCnrMutChr";
            this.nudCnrMutChr.Size = new System.Drawing.Size(71, 20);
            this.nudCnrMutChr.TabIndex = 21;
            // 
            // lblProcMut
            // 
            this.lblProcMut.AutoSize = true;
            this.lblProcMut.Location = new System.Drawing.Point(326, 165);
            this.lblProcMut.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProcMut.Name = "lblProcMut";
            this.lblProcMut.Size = new System.Drawing.Size(198, 13);
            this.lblProcMut.TabIndex = 22;
            this.lblProcMut.Text = "Вероятность мутации (в процентах %)";
            // 
            // nudMutProc
            // 
            this.nudMutProc.Location = new System.Drawing.Point(527, 165);
            this.nudMutProc.Margin = new System.Windows.Forms.Padding(2);
            this.nudMutProc.Name = "nudMutProc";
            this.nudMutProc.Size = new System.Drawing.Size(74, 20);
            this.nudMutProc.TabIndex = 23;
            // 
            // AddItemBtn
            // 
            this.AddItemBtn.Location = new System.Drawing.Point(113, 14);
            this.AddItemBtn.Name = "AddItemBtn";
            this.AddItemBtn.Size = new System.Drawing.Size(75, 23);
            this.AddItemBtn.TabIndex = 24;
            this.AddItemBtn.Text = "Добавить";
            this.AddItemBtn.UseVisualStyleBackColor = true;
            this.AddItemBtn.Click += new System.EventHandler(this.AddItemBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(686, 470);
            this.Controls.Add(this.AddItemBtn);
            this.Controls.Add(this.nudMutProc);
            this.Controls.Add(this.lblProcMut);
            this.Controls.Add(this.nudCnrMutChr);
            this.Controls.Add(this.lblCntMutChr);
            this.Controls.Add(this.nudCntParChr);
            this.Controls.Add(this.lblCntParChr);
            this.Controls.Add(this.nudCntChr);
            this.Controls.Add(this.lblCntChr);
            this.Controls.Add(this.nudCntSteps);
            this.Controls.Add(this.lblCntSteps);
            this.Controls.Add(this.lblForGen);
            this.Controls.Add(this.tbTimeGen);
            this.Controls.Add(this.lblTimeGen);
            this.Controls.Add(this.tbTimePerm);
            this.Controls.Add(this.lblTimePerm);
            this.Controls.Add(this.LVGen);
            this.Controls.Add(this.lblGen);
            this.Controls.Add(this.tbMaxW);
            this.Controls.Add(this.lblMaxWeigth);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.lblPerm);
            this.Controls.Add(this.lblOrig);
            this.Controls.Add(this.LVPermut);
            this.Controls.Add(this.OrigLV);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.Text = "Main Form";
            ((System.ComponentModel.ISupportInitialize)(this.nudCntSteps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCntChr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCntParChr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCnrMutChr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMutProc)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView OrigLV;
        private System.Windows.Forms.ListView LVPermut;
        private System.Windows.Forms.Label lblOrig;
        private System.Windows.Forms.Label lblPerm;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Label lblMaxWeigth;
        private System.Windows.Forms.TextBox tbMaxW;
        private System.Windows.Forms.ColumnHeader Name_1;
        private System.Windows.Forms.ColumnHeader Weigth;
        private System.Windows.Forms.ColumnHeader Price;
        private System.Windows.Forms.ColumnHeader Name_2;
        private System.Windows.Forms.ColumnHeader Weigth_1;
        private System.Windows.Forms.ColumnHeader Price_1;
        private System.Windows.Forms.Label lblGen;
        private System.Windows.Forms.ListView LVGen;
        private System.Windows.Forms.ColumnHeader Name_3;
        private System.Windows.Forms.ColumnHeader Weigth_3;
        private System.Windows.Forms.ColumnHeader Price_3;
        private System.Windows.Forms.Label lblTimePerm;
        private System.Windows.Forms.TextBox tbTimePerm;
        private System.Windows.Forms.Label lblTimeGen;
        private System.Windows.Forms.TextBox tbTimeGen;
        private System.Windows.Forms.Label lblForGen;
        private System.Windows.Forms.Label lblCntSteps;
        private System.Windows.Forms.NumericUpDown nudCntSteps;
        private System.Windows.Forms.Label lblCntChr;
        private System.Windows.Forms.NumericUpDown nudCntChr;
        private System.Windows.Forms.Label lblCntParChr;
        private System.Windows.Forms.NumericUpDown nudCntParChr;
        private System.Windows.Forms.Label lblCntMutChr;
        private System.Windows.Forms.NumericUpDown nudCnrMutChr;
        private System.Windows.Forms.Label lblProcMut;
        private System.Windows.Forms.NumericUpDown nudMutProc;
        public System.Windows.Forms.Button AddItemBtn;
    }
}

