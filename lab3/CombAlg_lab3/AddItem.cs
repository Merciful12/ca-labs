﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CombAlg_lab3
{
    public partial class AddItem : Form
    {
        public Item item;
        public AddItem()
        {
            InitializeComponent();
        }

        public void btnAdd_Click(object sender, EventArgs e)
        {
            MainForm main = this.Owner as MainForm;
            string name = nameBox.Text;
            int weigth = int.Parse(weigthBox.Text);
            int price = int.Parse(priceBox.Text);
            item = new Item(name, weigth, price);
            if (main != null && item != null)
                main.items.Add(item);
            Close();
        }
    }
}
