﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CombAlg_lab3
{
    public partial class MainForm : Form
    {
        public List<Item> items = new List<Item>();
        public MainForm()
        {
            InitializeComponent();
        }
        public void ShowItems(List<Item> it)
        {
            OrigLV.Items.Clear();
            LVPermut.Items.Clear();
            foreach (Item i in it)
            {
                OrigLV.Items.Add(new ListViewItem(new string[] { i.name, i.weigth.ToString(), i.price.ToString() }));
            }
        }

        public void btnRun_Click(object sender, EventArgs e)
        {
            Backpack bp = new Backpack(Convert.ToDouble(tbMaxW.Text));
            bp.MakeAllSets(items);
            List<Item> solve = bp.GetBestSet();
            if (solve == null)
                MessageBox.Show("Нет решения простым перебором!");
            else
            {
                LVPermut.Items.Clear();
                foreach (Item i in solve)
                {
                    LVPermut.Items.Add(new ListViewItem(new string[] { i.name, i.weigth.ToString(), i.price.ToString() }));
                }
                tbTimePerm.Text = bp.CurrTime.ToString();
            }
            GeneticAlgorithm ga = new GeneticAlgorithm(Convert.ToDouble(tbMaxW.Text), items, (int)nudMutProc.Value, (int)nudCntChr.Value, (int)nudCnrMutChr.Value, (int)nudCntParChr.Value);
            ga.RunGenAlgorithm((int)nudCntSteps.Value);
            List<Item> gen_solve = ga.bestItems;
            if (gen_solve == null)
                MessageBox.Show("Нет решения генетическим алгоритмом!");
            else
            {
                LVGen.Items.Clear();
                foreach (Item i in gen_solve)
                {
                    LVGen.Items.Add(new ListViewItem(new string[] { i.name, i.weigth.ToString(), i.price.ToString() }));
                }
                tbTimeGen.Text = ga.TimeGen.ToString();
            }
        }

        public void AddItemBtn_Click(object sender, EventArgs e)
        {
            AddItem AddForm = new AddItem();
            AddForm.Owner = this;
            AddForm.ShowDialog();
            ShowItems(items);
        }
    }
}
